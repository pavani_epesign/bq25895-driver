/*******************************************************************************
 BQ25895 I2C Driver: Defines Header File

 File Name:
 bq25895.h

 Summary:
 This header file contains object declarations used in the API.
 This also contains device specific defines and register address defines.
 This header file provides the Register Level Function Prototypes for the
 BQ25895 Battery Charger.

 Description:
 None.
 *******************************************************************************/

#ifndef BQ25895_H_
#define BQ25895_H_

// *****************************************************************************

// Included Files

#include <stdint.h>

// *****************************************************************************

// Register Addresses

#define BQ25895_REG00 0x00
#define BQ25895_REG01 0x01
#define BQ25895_REG02 0x02
#define BQ25895_REG03 0x03
#define BQ25895_REG04 0x04
#define BQ25895_REG05 0x05
#define BQ25895_REG06 0x06
#define BQ25895_REG07 0x07
#define BQ25895_REG08 0x08
#define BQ25895_REG09 0x09
#define BQ25895_REG0A 0x0A
#define BQ25895_REG0B 0x0B               /* Read Only */
#define BQ25895_REG0C 0x0C               /* Read Only */
#define BQ25895_REG0D 0x0D
#define BQ25895_REG0E 0x0E               /* Read Only */
#define BQ25895_REG0F 0x0F               /* Read Only */
#define BQ25895_REG10 0x10               /* Read Only */
#define BQ25895_REG11 0x11               /* Read Only */
#define BQ25895_REG12 0x12               /* Read Only */
#define BQ25895_REG13 0x13               /* Read Only */
#define BQ25895_REG14 0x14

// *****************************************************************************

//OffSet Values

#define REG00_IINLIM_OS_mA         100
#define REG03_SYS_MIN_OS_V           3
#define REG04_ICHG_OS_mA             0
#define REG05_IPRECHG_OS_mA         64
#define REG05_ITERM_OS_mA           64
#define REG06_VREG_OS_mV          3840
#define REG08_VCLAMP_OS_mV           0
#define REG0A_BOOSTV_OS_mV        4550
#define REG0D_VINDPM_OS_mV        2600
#define REG0E_BATV_OS_mV          2304
#define REG0F_SYSV_OS_mV          2304
#define REG10_TSPCT_OS_Percent      21
#define REG11_VBUSV_OS_mV         2600
#define REG12_ICHGR_OS_mA            0
#define REG13_IDPM_LIM_OS_mV       100

// *****************************************************************************

// Default/Reset Values of Register Bits

#define REG00_IINLIM_500mA                0x08
#define REG00_EN_ILIM                        1
#define REG01_VINDPM_OS_500mV             0x05
#define REG02_BOOST_FREQ                     1
#define REG02_ICO_EN                         1
#define REG02_HVDCP_EN                       1
#define REG02_MAXC_EN                        1
#define REG02_AUTO_DPDM_EN                   1
#define REG03_OTG_CONFIG                     1
#define REG03_CHG_CONFIG                     1
#define REG03_SYS_MIN_3500mV              0x05
#define REG04_ICHG_2048mA                 0x20
#define REG05_IPRECHG_128mA               0x01
#define REG05_ITERM_256mA                 0x03
#define REG06_VREG_4208mV                 0x17
#define REG06_BATLOWV                        1
#define REG07_EN_TERM                        1
#define REG07_WATCHDOG_40s                0x01
#define REG07_EN_TIMER                       1
#define REG07_CHG_TIMER_12hrs             0x02
#define REG08_TREG_120C                   0x03
#define REG09_TMR2X_EN                       1
#define REG09_BATFET_RST_EN                  1
#define REG0A_BOOSTV_5126mV               0x09
#define REG0D_VINDPM_4400mV               0x12


// *****************************************************************************

//Object Declarations

/* REG00 Input Current Limit values */

enum BQ25895_INPUT_CURRENT_LIMIT
{
  IINLIM_1600mA = 0x20,
  IINLIM_800mA = 0x10,
  IINLIM_400mA = 0x08,
  IINLIM_200mA = 0x04,
  IINLIM_100mA = 0x02,
  IINLIM_50mA = 0x01,

};

/*  REG01 Input Voltage Limit Offset values */

enum BQ25895_INPUT_VOLTAGE_LIMIT_OFFSET
{
  VINDPM_OS_1600mV = 0x20,
  VINDPM_OS_800mV = 0x10,
  VINDPM_OS_400mV = 0x08,
  VINDPM_OS_200mV = 0x04,
  VINDPM_OS_100mV = 0x02,
  VINDPM_OS_50mV = 0x01,

};

/*  REG01 Hot Temperature Monitor Threshold in Boost Mode */

enum BQ25895_BOOSTM_HOTTEMP_THRD
{
  VBHOT1_Threshold = 0x00,
  VBHOT0_Threshold = 0x40,
  VBHOT2_Threshold = 0x80,
  Disable_thermal_protect = 0xC0,
};

/*  REG01 Cold Temperature Monitor Threshold in Boost Mode */

enum BQ25895_BOOSTM_COLDTEMP_THRD
{
  VBCOLD0_Threshold = 0x00, VBCOLD1_Threshold = 0x20,
};

/*  REG03 Minimum System Voltage Limit */

enum BQ25895_MIN_SYS_VLIMIT
{
  SYS_MIN_400mV = 0x04, SYS_MIN_200mV = 0x02, SYS_MIN_100mV = 0x01,
};

/*  REG04 Fast Charge Current Limit */

enum BQ25895_FAST_CHARGE_CURRENT_LIMIT
{
  ICHG_4096mA = 0x40,
  ICHG_2048mA = 0x20,
  ICHG_1024mA = 0x10,
  ICHG_512mA = 0x08,
  ICHG_256mA = 0x04,
  ICHG_128mA = 0x02,
  ICHG_64mA = 0x01,
  ICHG_0mA = 0x00,

};

/*  REG05 Precharge Current Limit */

enum BQ25895_PRECHARGE_CURRENT_LIMIT
{
  IPRECHG_512mA = 0x80,
  IPRECHG_256mA = 0x40,
  IPRECHG_128mA = 0x20,
  IPRECHG_64mA = 0x10,

};

/*  REG05 Termination Current Limit */

enum BQ25895_TERMINATION_CURRENT_LIMIT
{
  ITERM_512mA = 0x08, ITERM_256mA = 0x04, ITERM_128mA = 0x02, ITERM_64mA = 0x01,
};

/*  REG06 Charge Voltage Limit */

enum BQ25895_CHARGE_VOLTAGE_LIMIT
{
  VREG_512mV = 0x80,
  VREG_256mV = 0x40,
  VREG_128mV = 0x20,
  VREG_64mV = 0x10,
  VREG_32mV = 0x08,
  VREG_16mV = 0x04,

};

/*  REG06 Battery Recharge Threshold Offset */

enum BQ25895_BAT_RCHRG_THRESD_Offset
{
  VRECHG_100mV = 0x00, VRECHG_200mV = 0x01,

};

/*  REG07 I2C Watchdog Timer Setting */

enum BQ25895_I2C_WATCHDOG_TIMER_Set
{
  WATCHDOG_DIS = 0x00,
  WATCHDOG_40s = 0x02,
  WATCHDOG_80s = 0x04,
  WATCHDOG_160s = 0x30,

};

/*  REG07 Fast Charge Timer Setting */

enum BQ25895_FAST_CHARGE_TIMER_Set
{
  CHG_TIMER_5hrs = 0x00,
  CHG_TIMER_8hrs = 0x02,
  CHG_TIMER_12hrs = 0x04,
  CHG_TIMER_20hrs = 0x06,

};

/*  REG08 IR Compensation Resistor Setting */

enum BQ25895_IR_COMP_RESIST_Set
{
  BAT_COMP_80mOhms = 0x80, BAT_COMP_40mOhms = 0x40, BAT_COMP_20mOhms = 0x20,

};

/*  REG08 IR Compensation Voltage Clamp */

enum BQ25895_IR_COMP_VOLTAGE_Set
{
  VCLAMP_128mV = 0x10, VCLAMP_64mV = 0x08, VCLAMP_32mV = 0x04,
};

/*  REG08 Thermal Regulation Threshold */

enum BQ25895_THERMAL_REG_Threshold

{
  TREG_60C = 0x00, TREG_80C = 0x01, TREG_100C = 0x02, TREG_120C = 0x03,
};

/*  REG0A Boost Mode Voltage Regulation */

enum BQ25895_BOOST_MOD_VOLTAGE_REG

{
  BOOSTV_512mV = 0x80,
  BOOSTV_256mV = 0x40,
  BOOSTV_128mV = 0x20,
  BOOSTV_64mV = 0x10,
};

/*  REG0B VBUS Status register */

enum BQ25895_VBUS_STATUS

{
  NO_INPUT = 0x00,
  USB_HOST_SDP = 0x20,
  USB_CDP = 0x40,
  USB_DCP = 0x60,
  ADJUSTABLE_HV_DCP = 0x80,
  UNKNOWN_ADAPTER = 0xA0,
  NON_STANDARD_ADAPTER = 0xC0,
  OTG = 0xE0
};

/*  REG0B Charging Status */

enum BQ25895_CHRG_STATUS

{
  NOT_CHARGING = 0x00,
  PRE_CHARGE = 0x08,
  FAST_CHARGING = 0x10,
  CHARGE_TERMINATION_DONE = 0x18
};

/*  REG0C Charge Fault Status */

enum BQ25895_CHARGING_FAULT
{
  NORMAL = 0x00,
  INPUT_FAULT = 0x10,
  THERMAL_SHUTDOWN = 0x20,
  CHARGE_SAFETY_TIMER_EXPIRATION = 0x30
};

/*  REG0C NTC Fault Status */

enum BQ25895_NTC_FAULT
{
  NORMALBB = 0x00,
  Buck_TS_COLD = 0x01,
  Buck_TS_HOT = 0x02,
  Boost_TS_COLD = 0x05,
  Boost_TS_HOT = 0x06,
};

/*  REG0D Absolute VINDPM Threshold */

enum BQ25895_ABS_VINDPM_Threshold

{
  VINDPM_6400mV = 0x40,
  VINDPM_3200mV = 0x20,
  VINDPM_1600mV = 0x10,
  VINDPM_800mV = 0x08,
  VINDPM_400mV = 0x04,
  VINDPM_200mV = 0x02,
  VINDPM_100mV = 0x01,
};

// *****************************************************************************

//FUNCTION PROTOTYPES


/* This Function provides High Impedance status of BQ25895 charger.
 * Using this, REGN LDO status is determined */
uint8_t
Get_bq25895_HIZ_mode ();


/* This Function is for setting High Impedance mode of BQ25895 charger.
 * Make the battery power Up the system by setting High Impedance mode.
 * #value is 0 (enables HIZ mode) and 1 (disables HIZ mode)*/
void
Set_bq25895_HIZ_mode (uint8_t value);


/* This Function provides the status of ILIM Pin of BQ25895 charger. */
uint8_t
Get_bq25895_EN_ILIM ();


/* This Function Enables/disables ILIM Pin of BQ25895 charger.
 * #value is 0 (enables ILIM Pin) and 1 (disables ILIM Pin) */
void
Set_bq25895_EN_ILIM(uint8_t value);


/* This Function provides Input current limit value of BQ25895 charger.
   Before reading the values, Enable the ILIM pin using this reference
   #REG00_EN_ILIM. If already enabled,Ignore this. */
uint8_t
Get_bq25895_IN_ILIM (uint8_t En_ILIM);


/* This Function is for setting Input current limit value of BQ25895 charger.
 * Note that It shouldnot be less than 500mA which makes ILIM Pin disable.
 * OFFSET:100mA and RESOLUTION:100mA
 * Set the values using this reference #BQ25895_INPUT_CURRENT_LIMIT*/
void
Set_bq25895_IN_ILIM (enum BQ25895_INPUT_CURRENT_LIMIT);


/* This Function provides Input Voltage limit Offset value of BQ25895 charger.
   The VINDPM threshold setting value is relative to this offset value. */
uint8_t
Get_bq25895_IN_VLIM_OS ();


/* This Function is for setting Input Voltage limit value of BQ25895 charger.
   The VINDPM threshold setting value is relative to this offset value.
 * Set the values using this reference #BQ25895_INPUT_VOLTAGE_LIMIT_OFFSET*/
void
Set_bq25895_IN_VLIM_OS (enum BQ25895_INPUT_VOLTAGE_LIMIT_OFFSET);


/* This Function provides Boost Mode Hot Temperature Monitor Threshold value
   of BQ25895 charger. */
enum BQ25895_BOOSTM_HOTTEMP_THRD
Get_bq25895_Hot_temp ();


/* This Function provides Boost Mode Cold Temperature Monitor Threshold value
   of BQ25895 charger. */
enum BQ25895_BOOSTM_COLDTEMP_THRD
Get_bq25895_Cold_temp ();


/*This Function is for setting Boost Mode Hot and Cold Temperature Monitor
  Threshold value of BQ25895 charger.
 * Set the values using this reference #BQ25895_BOOSTM_HOTTEMP_THRD and
   #BQ25895_BOOSTM_COLDTEMP_THRD*/
void
Set_bq25895_BM_temp (enum BQ25895_BOOSTM_HOTTEMP_THRD,
                     enum BQ25895_BOOSTM_COLDTEMP_THRD);


/* This Function provides Conversion start status of ADC of BQ25895 charger.
 * This bit is read-only when CONV_RATE = 1. The bit stays high during ADC
   conversion and during input source detection. */
uint8_t
get_bq25895_ADC_CONV_START ();


/* ADC Conversion Start Control
 * #value is 0 – ADC conversion not active (default) and 1 – Start ADC
    Conversion */
void
Set_bq25895_ADC_CONV_START (uint8_t value );


/* This Function provides Conversion RATE status of ADC of BQ25895 charger. */
uint8_t
get_bq25895_ADC_CONV_RATE ();


/* ADC Conversion Rate Selection
 * #value is 0 – One shot ADC conversion (default) and 1 – Start 1s
   Continuous Conversion */
void
Set_bq25895_ADC_CONV_RATE (uint8_t value);


/* This Function provides device frequency in boost mode of BQ25895 charger. */
uint8_t
Get_bq25895_BOOST_FREQ ();


/* Boost Mode Frequency Selection
 * #value is 0 – 1.5MHz and 1 – 500KHz (default)
 *Note: Write to this bit is ignored when OTG_CONFIG is enabled */
void
Set_bq25895_BOOST_FREQ (uint8_t value);


/* This Function provides status of Input Current Optimizer of BQ25895 charger. */
uint8_t
Get_bq25895_ICO_EN ();


/*Input Current Optimizer (ICO) Enable
 * #value is 0 – Disable ICO Algorithm or
    1 – Enable ICO Algorithm (default) */
void
Set_bq25895_ICO_EN (uint8_t value);


/* This Function provides status of InSource_Type of BQ25895 charger. */
void
Get_bq25895_InSource_Type (uint8_t *HDCP_En, uint8_t *MaxC_En,
                           uint8_t *FORCE_DPDM, uint8_t *AUTO_DPDM_EN);


/*This Function Enables/disables High Voltage DCP of BQ25895 charger.
 * #value is 0 (disable) and 1 (enable) */
void
Set_bq25895_InSource_HDCP (uint8_t value);


/*This Function Enables/disables MaxCharge Adapte of BQ25895 charger.
 * #value is 0 (disable) and 1 (enable) */
void
Set_bq25895_InSource_MaxC (uint8_t value);


/*This Function Enables/disables Force D+/D- Detection of BQ25895 charger.
 * #value is 0 (– Not in D+/D- or PSEL detection ) and
     1 (Force D+/D- detectione) */
void
Set_bq25895_InSource_FORCE_DPDM (uint8_t value);


/*This Function Enables/disables Automatic D+/D- Detection of BQ25895 charger.
 * #value is 0 (disable) and 1 (enable) */
void
Set_bq25895_InSource_AUTO_DPDM (uint8_t value);


/* This Function provides status of Battery Load of BQ25895 charger. */
uint8_t
Get_bq25895_BAT_LOAD ();


/*This Function Enables/disables Battery Load of BQ25895 charger.
 * #value is 0 (disable) and 1 (enable) */
void
Set_bq25895_BAT_LOAD (uint8_t value);


/* This Function provides status of Watchdog Timer of BQ25895 charger. */
uint8_t
Get_bq25895_WD_RST ();


/*I2C Watchdog Timer Reset
 * #value is 0 – Normal (default) or
    1 – Reset (Back to 0 after timer reset) */
void
Set_bq25895_WD_RST (uint8_t value);


/* This Function provides Boost (OTG) Mode Configuration of BQ25895 charger. */
uint8_t
Get_bq25895_OTG_CONFIG ();


/*This Function Enables/disables Boost (OTG) Mode Configuration of BQ25895 charger.
 * #value is 0 (disables) and 1 (enables) */
void
Set_bq25895_OTG_CONFIG (uint8_t value);


/* This Function provides Charge Enable Configuration of BQ25895 charger. */
uint8_t
Get_bq25895_CHG_CONFIG ();


/*This Function Enables/disables Charge Enable Configuration of BQ25895 charger.
 * #value is 0 (disables) and 1 (enables) */
void
Set_bq25895_CHG_CONFIG (uint8_t value);


/* This Function provides Minimum System Voltage Limit value of BQ25895 charger. */
uint8_t
Get_bq25895_SYS_VLMIN ();


/* This Function is for setting Minimum System Voltage Limit value of BQ25895
   charger.
 * Set the values using this reference #BQ25895_MIN_SYS_VLIMIT
  * OFFSET:3V and RESOLUTION: 10V */
void
Set_bq25895_SYS_VLMIN (enum BQ25895_MIN_SYS_VLIMIT);


/* This Function provides status of Current pulse control of BQ25895 charger. */
uint8_t
Get_bq25895_PUMPX_EN ();


/*This Function is for Enabling/disabling Current pulse control of BQ25895 charger.
 * #value is 0 (disables) or 1 (enables) */
void
Set_bq25895_PUMPX_EN (uint8_t value);


/* This Function provides Fast Charge Current Limit value of BQ25895 charger. */
uint8_t
Get_bq25895_ICHG ();


/*This Function is for setting Fast Charge Current Limit value of BQ25895
  charger.
 * Set the values using this reference #BQ25895_FAST_CHARGE_CURRENT_LIMIT */
void
Set_bq25895_ICHG (enum BQ25895_FAST_CHARGE_CURRENT_LIMIT);


/* This Function provides battery current limit values of BQ25895 charger. */
void
Get_bq25895_ILCHG (uint8_t IPRECHG, uint8_t ITERM);


/*This Function is for setting Precharge Current Limit value of BQ25895 charger.
 * Set the values using this reference #BQ25895_PRECHARGE_CURRENT_LIMIT
 * OFFSET:64mA and RESOLUTION: 64mA*/
void
Set_bq25895_IPRECHG (enum BQ25895_PRECHARGE_CURRENT_LIMIT);


/*This Function is for setting Termination Current Limit value of BQ25895
  charger.
 * Set the values using this reference #BQ25895_TERMINATION_CURRENT_LIMIT
 * OFFSET:64mA and RESOLUTION: 64mA */
void
Set_bq25895_ITERM (enum BQ25895_TERMINATION_CURRENT_LIMIT);


/* This Function provides Battery Recharge Threshold Offset value of BQ25895
   charger. */
enum BQ25895_BAT_RCHRG_THRESD_Offset
Get_bq25895_VRECHG ();


/* This Function provides voltage status of charger of BQ25895 charger. */
void
Get_bq25895_CHG_VSTAT (uint8_t VREG, uint8_t BATLOWV);


/*This Function is for setting Charge Voltage Limit value of BQ25895 charger.
 * Set the values using this reference #BQ25895_CHARGE_VOLTAGE_LIMIT */
void
Set_bq25895_VREG (enum BQ25895_CHARGE_VOLTAGE_LIMIT);


/*Battery Precharge to Fast Charge Threshold
 * #value is 0 – 2.8V or 1 – 3.0V (default) */
void
Set_bq25895_BATLOWV (uint8_t value);


/* This Function is for setting Battery Recharge Threshold Offset value
   of BQ25895 charger.
 * Set the values using this reference #BQ25895_BAT_RCHRG_THRESD_Offset*/
void
Set_bq25895_VRECHG (enum BQ25895_BAT_RCHRG_THRESD_Offset);


/* This Function provides status of Charging Termination of BQ25895 charger. */
uint8_t
Get_bq25895_EN_TERM ();


/*This Function Enables/disables Charging Termination of BQ25895 charger.
 * #value is 0 (disables) and 1 (enables) */
void
Set_bq25895_EN_TERM (uint8_t value);


/* This Function provides status of STAT Pin of BQ25895 charger. */
uint8_t
Get_bq25895_STAT_DIS ();


/* STAT Pin Disable
 * #value is 0 – Enable STAT pin function (default) or
             1 – Disable STAT pin function*/
void
Set_bq25895_STAT_DIS (uint8_t value);


/* This Function provides watchdog and safety timer status  of BQ25895
   charger. */
void
Get_bq25895_TIMER_STAT (uint8_t WATCHDOG, uint8_t EN_TIMER, uint8_t CHG_TIMER);


/*This Function is for setting I2C Watchdog Timer of BQ25895 charger.
 * Set the values using this reference #BQ25895_I2C_WATCHDOG_TIMER_Set */
void
Set_bq25895_WATCHDOG (enum BQ25895_I2C_WATCHDOG_TIMER_Set);


/* This Function is for setting Charging Safety Timer of BQ25895 charger.
 * #EN_TIMER is
   0 – Disable or
   1 – Enable (default)
 * Set the values using this reference #BQ25895_FAST_CHARGE_TIMER_Set*/
void
Set_bq25895_TIMER_STAT (uint8_t EN_TIMER , enum BQ25895_FAST_CHARGE_TIMER_Set);


/* This Function provides IR Compensation Resistor value and IR
   Compensation Voltage Clamp value of BQ25895 charger. */
void
Get_bq25895_IR_Comp_STAT (uint8_t BAT_COMP, uint8_t VCLAMP);


/* This Function is for setting IR Compensation Resistor value and IR
   Compensation Voltage Clamp value of BQ25895 charger.
 * Set the values using this reference #BQ25895_IR_COMP_RESIST_Set and
   #BQ25895_IR_COMP_VOLTAGE_Set
 * Resistance Offset : 0 ohms and Resolution : 20 ohms
 * voltage Offset : 0mV and Resolution : 32mV*/
void
Set_bq25895_IR_Comp (enum BQ25895_IR_COMP_RESIST_Set,
                     enum BQ25895_IR_COMP_VOLTAGE_Set);


/* This Function provides Thermal Regulation Threshold value of BQ25895 charger. */
uint8_t
Get_bq25895_TREG ();


/*This Function is for setting Thermal Regulation Threshold value of BQ25895
  charger.
 * Set the values using this reference #BQ25895_THERMAL_REG_Threshold */
void
Set_bq25895_TREG (enum BQ25895_THERMAL_REG_Threshold);


/* This Function provides status Input Current Optimizer of BQ25895 charger. */
uint8_t
Get_bq25895_FORCE_ICO ();


/* Force Start Input Current Optimizer (ICO)
 * #value is
    0 – Do not force ICO (default) or
    1 – Force ICO
    Note:
    This bit is can only be set only and always returns to 0 after ICO starts*/
void
Set_bq25895_FORCE_ICO (uint8_t value);


/* This Function provides status of Safety Timer of BQ25895 charger. */
uint8_t
Get_bq25895_TMR2X_EN ();


/*Safety Timer Setting during DPM or Thermal Regulation
 * #value is
  0 – Safety timer not slowed by 2X during input DPM or thermal
       regulation or
  1 – Safety timer slowed by 2X during input DPM or thermal regulation
       (default)
 */
void
Set_bq25895_TMR2X_EN (uint8_t value);


/* This Function provides status of BATFET of BQ25895 charger. */
void
Get_bq25895_BATFET_STAT (uint8_t BATFET_DLY, uint8_t BATFET_RST_EN);


/* This Function provides disable status of BATFET   of BQ25895 charger. */
uint8_t
Get_bq25895_BATFET_DIS ();


/*Force BATFET off to enable ship mode
 * #value is 0 – Allow BATFET turn on (default) or
         1 – Force BATFET off
 */
void
Set_bq25895_BATFET_DIS (uint8_t value);


/*BATFET turn off delay control
 * #value is
  0 – BATFET turn off immediately when BATFET_DIS bit is set (default) or
  1 – BATFET turn off delay by tSM_DLY when BATFET_DIS bit is set */
void
Set_bq25895_BATFET_DLY (uint8_t value );


/* This Function provides status of BATFET full system reset of
   BQ25895 charger. */
uint8_t
Get_bq25895_BATFET_RST_EN ();


/*This Function Enables/disables BATFET full system reset of BQ25895 charger.
 * #value is 0 (disable) and 1 (enable) */
void
Set_bq25895_BATFET_RST_EN (uint8_t value);


/* This Function provides Current pulse control voltage up/down of
   BQ25895 charger. */
void
Get_bq25895_PUMPX_STAT (uint8_t PUMPX_UP, uint8_t PUMPX_DN);


/*Current pulse control voltage up enable
 * #value is 0 – Disable (default) and 1 – Enable
 Note:
 This bit is can only be set when EN_PUMPX bit is set and returns to 0
 after current pulse control sequence is completed */
void
Set_bq25895_PUMPX_UP (uint8_t value);


/*Current pulse control voltage down enable
 * #value is 0 – Disable (default) and 1 – Enable
 Note:
  This bit is can only be set when EN_PUMPX bit is set and returns to 0
  after current pulse control sequence is completed. */
void
Set_bq25895_PUMPX_DN (uint8_t value);


/* This Function provides Boost Mode Voltage Regulation value of BQ25895
   charger. */
uint8_t
Get_bq25895_BOOSTV ();


/*This Function is for setting Boost Mode Voltage Regulation value of BQ25895
   charger.
 * Set the values using this reference #BQ25895_BOOST_MOD_VOLTAGE_REG
 * Offset :4550mV and Resolution: 64mV */
void
Set_bq25895_BOOSTV (enum BQ25895_BOOST_MOD_VOLTAGE_REG);


/* This Function provides Status of VBUS Register  of BQ25895 charger.
   000: No Input 001: USB Host SDP
   010: USB CDP (1.5A)
   011: USB DCP (3.25A)
   100: Adjustable High Voltage DCP (MaxCharge) (1.5A)
   101: Unknown Adapter (500mA)
   110: Non-Standard Adapter (1A/2A/2.1A/2.4A)
   111: OTG
   Note: Software current limit is reported in IINLIM register */
uint8_t
Get_bq25895_VBUS_STAT ();


/* This Function provides Charging Status of BQ25895 charger.
   00 – Not Charging
   01 – Pre-charge ( < VBATLOWV)
   10 – Fast Charging
   11 – Charge Termination Done*/
uint8_t
Get_bq25895_CHRG_STAT ();


/* This Function provides Power Good Status of BQ25895 charger.
    0 – Not Power Good
    1 – Power Good  */
uint8_t
Get_bq25895_PG_STAT ();


/* This Function provides USB Input Status of BQ25895 charger.
   0 – USB100 input is detected
   1 – USB500 input is detected
   Note: This bit always read 1 when VBUS_STAT is not 001 */
uint8_t
Get_bq25895_SDP_STAT ();


/* This Function provides VSYS Regulation Status of BQ25895 charger.
 *0 – Not in VSYSMIN regulation (BAT > VSYSMIN) and 1 – In VSYSMIN regulation
  (BAT < VSYSMIN)  */
uint8_t
Get_bq25895_VSYS_STAT ();


/* This Function provides Watchdog Fault Status of BQ25895 charger. */
uint8_t
Get_bq25895_WATCHDOG_FAULT ();


/* This Function provides Boost Mode Fault Status of BQ25895 charger. */
uint8_t
Get_bq25895_BOOST_FAULT ();


/* This Function provides Charge Fault Status of BQ25895 charger. */
enum BQ25895_CHARGING_FAULT
Get_bq25895_CHRG_FAULT ();


/* This Function provides Battery Fault Status of BQ25895 charger. */
uint8_t
Get_bq25895_BAT_FAULT ();


/* This Function provides NTC Fault Status of BQ25895 charger. */
enum BQ25895_NTC_FAULT
Get_bq25895_NTC_FAULT ();


/* VINDPM Threshold Setting Method
 * #value is 0 – Run Relative VINDPM Threshold (default) and 1 – Run Absolute
   VINDPM  Threshold */
void
Set_bq25895_FORCE_VINDPM (uint8_t value);


/* This Function provides device frequency in boost mode of BQ25895 charger. */
uint8_t
Get_bq25895_FORCE_VINDPM ();


/* This Function provides Absolute VINDPM Threshold value of BQ25895 charger.
 *Register is read only when FORCE_VINDPM=0 */
uint8_t
Get_bq25895_VINDPM ();


/*This Function is for setting Absolute VINDPM Threshold value of BQ25895
  charger.
 * OFFSET:2600mV and RESOLUTION:4400mV
 * Set the values using this reference #BQ25895_ABS_VINDPM_ThresholdT */
void
Set_bq25895_VINDPM (enum BQ25895_ABS_VINDPM_Threshold);


/* This Function provides Thermal Regulation Status of BQ25895 charger.
 *0 – Normal and 1 – In Thermal Regulation */
uint8_t
Get_bq25895_THERM_STAT ();


/* This Function provides ADC conversion of Battery Voltage (VBAT) of
    BQ25895 charger. */
uint8_t
Get_bq25895_ADC_Conv_BATV ();


/* This Function provides ADDC conversion of System Voltage (VSYS) of
   BQ25895 charger. */
uint8_t
Get_bq25895_ADC_Conv_SYSV ();


/* This Function provides ADC conversion of TS Voltage (TS) as percentage of
    REGN of BQ25895 charger. */
uint8_t
Get_bq25895_ADC_Conv_TSV ();


/* This Function provides ADC conversion of VBUS voltage (VBUS) of BQ25895
   charger. */
uint8_t
Get_bq25895_ADC_Conv_VBUSV ();


/* This Function provides ADC conversion of Charge Current (IBAT) when VBAT >
    VBATSHORT of BQ25895 charger. */
uint8_t
Get_bq25895_ADC_Conv_ICHGR ();


/* This Function provides VINDPM Status and IINDPM Status of BQ25895 charger.
 *0 – Not in VINDPM and 1 – VINDPM
 *0 – Not in IINDPM and 1 – IINDPM */
void
Get_bq25895_IN_REG_STAT (uint8_t VDPM_STAT, uint8_t IDPM_STAT);


/* This Function provides Input Current Limit in effect while Input
   Current Optimizer (ICO) is enabled of BQ25895 charger. */
uint8_t
Get_bq25895_IDPM_LIM ();


/* This Function provides Register Reset status of BQ25895 charger. */
uint8_t
Get_bq25895_REG_RST ();


/*Register Reset
 *0 – Keep current register setting (default) and 1 – Reset to default register
  value and reset safety timer
 *Note: Reset to 0 after register reset is completed */
void
Set_bq25895_REG_RST (uint8_t value);


/* This Function provides Device Configuration and Device Revision of
   BQ25895 charger.
 * 111: BQ25895
 * Device Revision: 01  */
void
Get_bq25895_DEVICE_INFO (uint8_t PN, uint8_t DEV_REV);


/* This Function provides Input Current Optimizer (ICO) Status of
   BQ25895 charger. */
uint8_t
Get_bq25895_ICO_OPTIMIZED ();


/* Input Current Optimizer (ICO) Status
 *0 – Optimization is in progress and 1 – Maximum Input Current Detected */
void
Set_bq25895_ICO_OPTIMIZED (uint8_t value);


/* This Function provides Temperature Profile of BQ25895 charger.
 *0 – Cold/Hot (default)  */
uint8_t
Get_bq25895_TS_PROFILE ();

#endif /* BQ25895_H_ */
