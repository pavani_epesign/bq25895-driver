/*******************************************************************************
 BQ25895 I2C Driver: Implementation

 File Name:
 bq25895.c

 Summary:

 This Source file provides Implementation of the all Register Level Functions
 of the BQ25895 Battery Charger.

 Description:
 None.

 *******************************************************************************/
// *****************************************************************************
// Includes
#include "bq25895.h"
#include "i2c.h"

// *****************************************************************************

// Global Variable declarations

typedef enum BQ25895_INPUT_CURRENT_LIMIT IINLIM;
typedef enum BQ25895_INPUT_VOLTAGE_LIMIT_OFFSET VINDPM_OS;
typedef enum BQ25895_BOOSTM_HOTTEMP_THRD Hot_temp;
typedef enum BQ25895_BOOSTM_COLDTEMP_THRD Cold_temp;
typedef enum BQ25895_MIN_SYS_VLIMIT SYS_MIN;
typedef enum BQ25895_FAST_CHARGE_CURRENT_LIMIT ICHG;
typedef enum BQ25895_PRECHARGE_CURRENT_LIMIT IPRECHG;
typedef enum BQ25895_TERMINATION_CURRENT_LIMIT ITERM;
typedef enum BQ25895_CHARGE_VOLTAGE_LIMIT VREG;
typedef enum BQ25895_BAT_RCHRG_THRESD_Offset VRECHG;
typedef enum BQ25895_I2C_WATCHDOG_TIMER_Set WATCHDOG;
typedef enum BQ25895_FAST_CHARGE_TIMER_Set CHG_TIMER;
typedef enum BQ25895_IR_COMP_RESIST_Set BAT_COMP;
typedef enum BQ25895_IR_COMP_VOLTAGE_Set VCLAMP;
typedef enum BQ25895_THERMAL_REG_Threshold TREG;
typedef enum BQ25895_BOOST_MOD_VOLTAGE_REG BOOSTV;
typedef enum BQ25895_CHARGING_FAULT CHRG_FAULT;
typedef enum BQ25895_NTC_FAULT NTC_FAULT;
typedef enum BQ25895_ABS_VINDPM_Threshold VINDPM;

// *****************************************************************************

// I2C Register level functions

uint8_t
Get_bq25895_HIZ_mode ()
{

  uint8_t rd, val, Reg_addr = BQ25895_REG00;

  rd = readByte (Reg_addr);

  //Clearing
  val &= ~(0x01);

  // Updating data
  val |= (rd >> 7) & 0x01;

  return val;

}

void
Set_bq25895_HIZ_mode (uint8_t value)
{

  uint8_t rd, Reg_addr = BQ25895_REG00;

  // getting current status
  rd = Get_bq25895_HIZ_mode ();

  //clearing the bit
  rd &= ~(0x80);

  //Updating
  rd = (value << 7) & 0x80;
  writeByte (Reg_addr, rd);

}

uint8_t
Get_bq25895_EN_ILIM ()
{

  uint8_t rd, val, Reg_addr = BQ25895_REG00;

  rd = readByte (Reg_addr);

  //Clearing
  val &= ~(0x01);

  // Updating data
  val |= (rd >> 6) & 0x01;

  return val;

}

void
Set_bq25895_EN_ILIM (uint8_t value)
{

  uint8_t rd, Reg_addr = BQ25895_REG00;

  // getting current status
  rd = Get_bq25895_EN_ILIM ();

  //clearing the bit
  rd &= ~(0x40);

  //Updating
  rd = (value << 6) & 0x40;
  writeByte (Reg_addr, rd);

}

uint8_t
Get_bq25895_IN_ILIM (uint8_t En_ILIM)
{

  uint8_t rd, val, Reg_addr = BQ25895_REG00;

  //Enabling ILIM pin
  En_ILIM = (En_ILIM << 6) & 0x40;
  writeByte (Reg_addr, En_ILIM);

  rd = readByte (Reg_addr);

  //Clearing
  val &= ~(0x3f);

  //Updating
  val |= rd & 0x3f;
  val = (val * 100) + REG00_IINLIM_OS_mA;   //OFFSET:100mA and RESOLUTION:100mA
  return val;                                // in mA

}

void
Set_bq25895_IN_ILIM (IINLIM value)
{
  uint8_t rd, Reg_addr = BQ25895_REG00;

  // getting current status
  rd = Get_bq25895_EN_ILIM ();

  //clearing the bits
  rd &= ~(0x3f);

  //Updating
  rd = value;
  writeByte (Reg_addr, rd);

}

uint8_t
Get_bq25895_IN_VLIM_OS ()
{

  uint8_t rd, val, Reg_addr = BQ25895_REG01;

  rd = readByte (Reg_addr);

  //Clearing
  val &= ~(0x1f);

  //Updating
  val |= rd & 0x1f;

  // in mV
  return (val * 100);             //RESOLUTION:100mV

}

void
Set_bq25895_IN_VLIM_OS (VINDPM_OS value)
{

  uint8_t rd, Reg_addr = BQ25895_REG01;

  // getting current status
  rd = Get_bq25895_IN_VLIM_OS ();

  //clearing the bits
  rd &= ~(0x1f);

  //Updating
  rd = value;

  writeByte (Reg_addr, rd);

}

Hot_temp
Get_bq25895_Hot_temp ()
{

  Hot_temp rd;
  uint8_t val, Reg_addr = BQ25895_REG01;

  val = readByte (Reg_addr);

  switch (val)
    {
    case VBHOT1_Threshold:
      rd = VBHOT1_Threshold;
      break;
    case VBHOT0_Threshold:
      rd = VBHOT0_Threshold;
      break;
    case VBHOT2_Threshold:
      rd = VBHOT2_Threshold;
      break;
    case Disable_thermal_protect:
      rd = Disable_thermal_protect;
      break;
    }

  return rd;

}

Cold_temp
Get_bq25895_Cold_temp ()
{

  Cold_temp rd;
  uint8_t val, Reg_addr = BQ25895_REG01;

  val = readByte (Reg_addr);

  switch (val)
    {

    case VBCOLD0_Threshold:
      rd = VBCOLD0_Threshold;
      break;
    case VBCOLD1_Threshold:
      rd = VBCOLD1_Threshold;
      break;

    }

  return rd;

}

void
Set_bq25895_BM_temp (Hot_temp BHT_value, Cold_temp BCT_value)
{

  uint8_t wd, Reg_addr = BQ25895_REG01;

  wd &= ~(0xE0);
  wd = (BHT_value << 7) | (BCT_value << 5);

  writeByte (Reg_addr, wd);

}

uint8_t
get_bq25895_ADC_CONV_START ()
{

  uint8_t rd, val, Reg_addr = BQ25895_REG02;

  //enabling 1s conversion
  writeByte (Reg_addr, 0x40);

  rd = readByte (Reg_addr);

  //Clearing
  val &= ~(0x01);

  //Updating
  val |= (rd >> 7) & 0x01;

  return val;

}

void
Set_bq25895_ADC_CONV_START (uint8_t value)
{

  uint8_t rd, Reg_addr = BQ25895_REG02;

  // getting current status
  rd = get_bq25895_ADC_CONV_START ();

  //clearing the bits
  rd &= ~(0x80);

  //Updating
  rd = (value << 7) & 0x80;

  writeByte (Reg_addr, rd);

}

uint8_t
get_bq25895_ADC_CONV_RATE ()
{

  uint8_t rd, val, Reg_addr = BQ25895_REG02;

  rd = readByte (Reg_addr);

  //Clearing
  val &= ~(0x01);

  //Updating
  val |= (rd >> 6) & 0x01;

  return val;

}

void
Set_bq25895_ADC_CONV_RATE (uint8_t value)
{

  uint8_t rd, Reg_addr = BQ25895_REG02;

  // getting current status
  rd = get_bq25895_ADC_CONV_RATE ();

  //clearing the bits
  rd &= ~(0x40);

  //Updating
  rd = (value << 6) & 0x40;

  writeByte (Reg_addr, rd);

}

uint8_t
Get_bq25895_BOOST_FREQ ()
{

  uint8_t val, rd, Reg_addr = BQ25895_REG02;

  rd = readByte (Reg_addr);

  //Clearing
  val &= ~(0x01);

  //Updating
  val |= (rd >> 5) & 0x01;

  return val;

}

void
Set_bq25895_BOOST_FREQ (uint8_t value)
{
  uint8_t wd, Reg_addr = BQ25895_REG02;

  // getting current status
  wd = Get_bq25895_BOOST_FREQ ();

  //clearing the bits
  wd &= ~(0x20);

  //Updating
  wd = (value << 5) & 0x20;

  writeByte (Reg_addr, value);

}

uint8_t
Get_bq25895_ICO_EN ()
{

  uint8_t val, rd, Reg_addr = BQ25895_REG02;

  rd = readByte (Reg_addr);

  //Clearing
  val &= ~(0x01);

  //Updating
  val |= (rd >> 4) & 0x01;
  return val;

}

void
Set_bq25895_ICO_EN (uint8_t value)
{

  uint8_t wd, Reg_addr = BQ25895_REG02;

  // getting current status
  wd = Get_bq25895_ICO_EN ();

  //Clearing
  wd &= ~(0x10);

  //Updating
  wd = (value << 4) & 0x10;

  writeByte (Reg_addr, value);

}

void
Get_bq25895_InSource_Type (uint8_t *HDCP_En, uint8_t *MaxC_En,
                           uint8_t *FORCE_DPDM, uint8_t *AUTO_DPDM_EN)
{

  uint8_t rd, Reg_addr = BQ25895_REG02;

  rd = readByte (Reg_addr);

  *HDCP_En = (rd >> 3) & 0x01;
  *MaxC_En = (rd >> 2) & 0x01;
  *FORCE_DPDM = (rd >> 1) & 0x01;
  *AUTO_DPDM_EN = rd & 0x01;
}

void
Set_bq25895_InSource_HDCP (uint8_t HDCP_En)
{
  uint8_t wd, Reg_addr = BQ25895_REG02;

  //clearing the bits
  wd &= ~(0x08);

  //Updating
  wd = (HDCP_En << 3) & 0x08;

  writeByte (Reg_addr, wd);
}

void
Set_bq25895_InSource_MaxC (uint8_t MaxC_En)
{

  uint8_t wd, Reg_addr = BQ25895_REG02;

  //Clearing
  wd &= ~(0x04);

  //Updating
  wd = (MaxC_En << 2) & 0x04;

  writeByte (Reg_addr, wd);

}

void
Set_bq25895_InSource_FORCE_DPDM (uint8_t FORCE_DPDM)
{

  uint8_t wd, Reg_addr = BQ25895_REG02;

  //Clearing
  wd &= ~(0x02);

  //Updating
  wd = (FORCE_DPDM << 1) & 0x02;

  writeByte (Reg_addr, wd);
}

void
Set_bq25895_InSource_AUTO_DPDM (uint8_t AUTO_DPDM_EN)
{
  uint8_t wd, Reg_addr = BQ25895_REG02;

  //Clearing
  wd &= ~(0x01);

  //Updating
  wd = AUTO_DPDM_EN;

  writeByte (Reg_addr, wd);
}

uint8_t
Get_bq25895_BAT_LOAD ()
{

  uint8_t rd, val, Reg_addr = BQ25895_REG03;

  rd = readByte (Reg_addr);

  //Clearing
  val &= ~(0x01);

  //Updating
  val |= (rd >> 7) & 0x01;
  return val;
}

void
Set_bq25895_BAT_LOAD (uint8_t value)
{
  uint8_t wd, Reg_addr = BQ25895_REG03;

  // getting current status
   wd = Get_bq25895_BAT_LOAD ();

  //clearing the bits
  wd &= ~(0x80);

  //Updating
  wd = (value << 7) & 0x80;

  writeByte (Reg_addr, wd);
}

uint8_t
Get_bq25895_WD_RST ()
{
  uint8_t rd, val, Reg_addr = BQ25895_REG03;

  rd = readByte (Reg_addr);

  //Clearing
  val &= ~(0x01);

  //Updating
  val |= (rd >> 6) & 0x01;
  return val;
}

void
Set_bq25895_WD_RST (uint8_t value)
{
  uint8_t wd, Reg_addr = BQ25895_REG03;

  // getting current status
   wd = Get_bq25895_WD_RST ();

  //clearing the bits
  wd &= ~(0x40);

  //Updating
  wd = (value << 6) & 0x40;

  writeByte (Reg_addr, wd);
}

uint8_t
Get_bq25895_OTG_CONFIG ()
{

  uint8_t rd, val, Reg_addr = BQ25895_REG03;

  rd = readByte (Reg_addr);

  //Clearing
  val &= ~(0x01);

  //Updating
  val |= (rd >> 5) & 0x01;
  return val;
}

void
Set_bq25895_OTG_CONFIG (uint8_t value)
{
  uint8_t wd, Reg_addr = BQ25895_REG03;

  // getting current status
   wd = Get_bq25895_OTG_CONFIG ();

  //clearing the bits
  wd &= ~(0x20);

  //Updating
  wd = (value << 5) & 0x20;

  writeByte (Reg_addr, wd);
}

uint8_t
Get_bq25895_CHG_CONFIG ()
{

  uint8_t rd, val, Reg_addr = BQ25895_REG03;

  rd = readByte (Reg_addr);

  //Clearing
  val &= ~(0x01);

  //Updating
  val |= (rd >> 4) & 0x01;
  return val;
}

void
Set_bq25895_CHG_CONFIG (uint8_t value)
{
  uint8_t wd, Reg_addr = BQ25895_REG03;

  // getting current status
   wd = Get_bq25895_CHG_CONFIG ();

  //clearing the bits
  wd &= ~(0x10);

  //Updating
  wd = (value << 4) & 0x10;

  writeByte (Reg_addr, wd);
}

uint8_t
Get_bq25895_SYS_VLMIN ()
{
  uint8_t rd, val, Reg_addr = BQ25895_REG03;

  rd = readByte (Reg_addr);

  //Clearing
  val &= ~(0x07);

  //Updating
  val |= (rd >> 1) & 0x07;
  val = (val + REG03_SYS_MIN_OS_V) / 10;                      // RESOLUTION:0.1V
  return val;
}

void
Set_bq25895_SYS_VLMIN (SYS_MIN value)
{
  uint8_t wd, Reg_addr = BQ25895_REG03;

  // getting current status
   wd = Get_bq25895_SYS_VLMIN ();

  //clearing the bits
  wd &= ~(0x0E);

  //Updating
  wd = (value << 1) & 0x0E;

  writeByte (Reg_addr, wd);
}

uint8_t
Get_bq25895_PUMPX_EN ()
{

  uint8_t rd, val, Reg_addr = BQ25895_REG04;

  rd = readByte (Reg_addr);

  //Clearing
  val &= ~(0x01);

  //Updating
  val |= (rd >> 7) & 0x01;
  return val;
}

void
Set_bq25895_PUMPX_EN (uint8_t value)
{
  uint8_t wd, Reg_addr = BQ25895_REG04;

  // getting current status
   wd = Get_bq25895_PUMPX_EN ();

  //clearing the bits
  wd &= ~(0x10);

  //Updating
  wd = (value << 7) & 0x10;

  writeByte (Reg_addr, wd);
}

uint8_t
Get_bq25895_ICHG ()
{

  uint8_t rd, val, Reg_addr = BQ25895_REG04;

  rd = readByte (Reg_addr);

  //Clearing
  val &= ~(0x7f);

  //Updating
  val |= rd & 0x7f;
  return val * 64;                 //RESOLUTION:64mA
}

void
Set_bq25895_ICHG (ICHG value)
{

  uint8_t wd, Reg_addr = BQ25895_REG04;

  // getting current status
   wd = Get_bq25895_ICHG ();

  //clearing the bits
  wd &= ~(0x7f);

  //Updating
  wd = value;

  writeByte (Reg_addr, wd);
}

void
Get_bq25895_ILCHG (uint8_t IPRECHG, uint8_t ITERM)
{
  uint8_t rd, Reg_addr = BQ25895_REG05;

  rd = readByte (Reg_addr);

  IPRECHG = (rd >> 4) & 0x0f;
  IPRECHG = (IPRECHG * 64) + REG05_IPRECHG_OS_mA;      // RESOLUTION:64mA

  ITERM = rd & 0x0f;
  ITERM = (ITERM * 64) + REG05_ITERM_OS_mA;           // RESOLUTION:64mA
}

void
Set_bq25895_IPRECHG (IPRECHG value)
{
  uint8_t wd, Reg_addr = BQ25895_REG05;

  //clearing the bits
  wd &= ~(0xf0);

  //Updating
  wd = value;

  writeByte (Reg_addr, wd);
}

void
Set_bq25895_ITERM (ITERM value)
{
  uint8_t wd, Reg_addr = BQ25895_REG05;

  //clearing the bits
  wd &= ~(0x0f);

  //Updating
  wd = value;

  writeByte (Reg_addr, wd);
}

void
Get_bq25895_CHG_VSTAT (uint8_t VREG, uint8_t BATLOWV)
{
  uint8_t rd, Reg_addr = BQ25895_REG06;

  rd = readByte (Reg_addr);

  VREG = (rd >> 2) & 0x3F;
  VREG = (VREG * 16) + REG06_VREG_OS_mV;                // RESOLUTION:16mV
  BATLOWV = (rd >> 1) & 0x01;
}

VRECHG
Get_bq25895_VRECHG ()
{
  VRECHG rd;
  uint8_t val, Reg_addr = BQ25895_REG06;

  val = readByte (Reg_addr);

  switch (val)
    {
    case VRECHG_100mV:
      rd = VRECHG_100mV;
      break;
    case VRECHG_200mV:
      rd = VRECHG_200mV;
      break;
    }
  return rd;
}

void
Set_bq25895_VREG (VREG value)
{
  uint8_t wd, Reg_addr = BQ25895_REG06;

  //clearing the bits
  wd &= ~(0x01);

  //Updating
  wd = value;

  writeByte (Reg_addr, wd);
}

void
Set_bq25895_BATLOWV (uint8_t value)
{
  uint8_t wd, Reg_addr = BQ25895_REG06;

  //clearing the bits
  wd &= ~(0x02);

  //Updating
  wd = (value << 1) & 0x02;

  writeByte (Reg_addr, wd);
}

void
Set_bq25895_VRECHG (VRECHG value)
{
  uint8_t wd, Reg_addr = BQ25895_REG06;

  //clearing the bits
  wd &= ~(0x01);

  //Updating
  wd = value & 0x01;

  writeByte (Reg_addr, wd);

}

uint8_t
Get_bq25895_EN_TERM ()
{
  uint8_t rd, val, Reg_addr = BQ25895_REG07;

  rd = readByte (Reg_addr);

  //Clearing
  val &= ~(0x01);

  //Updating
  val |= (rd >> 7) & 0x01;
  return val;
}

void
Set_bq25895_EN_TERM (uint8_t value)
{
  uint8_t wd, Reg_addr = BQ25895_REG07;

  // getting current status
   wd = Get_bq25895_EN_TERM ();

  //clearing the bits
  wd &= ~(0x80);

  //Updating
  wd = (value << 7) & 0x80;

  writeByte (Reg_addr, wd);

}
uint8_t
Get_bq25895_STAT_DIS ()
{

  uint8_t rd, val, Reg_addr = BQ25895_REG07;
  rd = readByte (Reg_addr);

  //Clearing
  val &= ~(0x01);

  //Updating
  val |= (rd >> 6) & 0x01;
  return val;
}

void
Set_bq25895_STAT_DIS (uint8_t value)
{
  uint8_t wd, Reg_addr = BQ25895_REG07;

  // getting current status
   wd = Get_bq25895_STAT_DIS ();

  //clearing the bits
  wd &= ~(0x40);

  //Updating
  wd = (value << 6) & 0x40;

  writeByte (Reg_addr, wd);
}

void
Get_bq25895_TIMER_STAT (uint8_t WATCHDOG, uint8_t EN_TIMER, uint8_t CHG_TIMER)
{

  uint8_t rd, Reg_addr = BQ25895_REG07;

  rd = readByte (Reg_addr);

  WATCHDOG = (rd >> 4) & 0x03;
  EN_TIMER = (rd >> 3) & 0x01;
  CHG_TIMER = (rd >> 1) & 0x03;
}

void
Set_bq25895_WATCHDOG (WATCHDOG value)
{
  uint8_t wd, Reg_addr = BQ25895_REG07;

  //clearing the bits
  wd &= ~(0x18);

  //Updating
  wd = (value << 4) & 0x18;

  writeByte (Reg_addr, wd);
}
void
Set_bq25895_TIMER_STAT (uint8_t EN_TIMER, CHG_TIMER value)
{
  uint8_t wd, Reg_addr = BQ25895_REG07;

  //clearing the bits
  wd &= ~(0x06);

  //Updating
  wd = ((EN_TIMER << 3) & 0x08) | ((value << 1) & 0x06);

}

void
Get_bq25895_IR_Comp_STAT (uint8_t BAT_COMP, uint8_t VCLAMP)
{
  uint8_t rd, Reg_addr = BQ25895_REG08;

  rd = readByte (Reg_addr);

  BAT_COMP = (rd >> 5) & 0x07;
  BAT_COMP = BAT_COMP * 20;                //RESOLUTION:20mΩ
  VCLAMP = (rd >> 2) & 0x07;
  VCLAMP = VCLAMP * 32;                    //RESOLUTION:32mV
}

void
Set_bq25895_IR_Comp (BAT_COMP resistance, VCLAMP voltage)
{
  uint8_t wd, Reg_addr = BQ25895_REG08;
  uint8_t BAT_COMPv,VCLAMPv;

  // getting current status
  Get_bq25895_IR_Comp_STAT (BAT_COMPv,VCLAMPv);
   wd = BAT_COMPv|VCLAMPv;

  //clearing the bits
  wd &= ~(0xfc);

  //Updating
  wd = resistance | voltage;

  writeByte (Reg_addr, wd);

}

uint8_t
Get_bq25895_TREG ()
{
  uint8_t rd, val, Reg_addr = BQ25895_REG08;

  rd = readByte (Reg_addr);

  //Clearing
  val &= ~(0x03);

  //Updating
  val |= rd & 0x03;
  return val;
}

void
Set_bq25895_TREG (TREG value)
{
  uint8_t wd, Reg_addr = BQ25895_REG08;

  // getting current status
   wd = Get_bq25895_TREG ();

  //clearing the bits
  wd &= ~(0x01);

  //Updating
  wd = value;

  writeByte (Reg_addr, wd);

}

uint8_t
Get_bq25895_FORCE_ICO ()
{
  uint8_t rd, val, Reg_addr = BQ25895_REG09;

  rd = readByte (Reg_addr);

  //Clearing
  val &= ~(0x01);

  //Updating
  val |= (rd >> 7) & 0x01;
  return val;
}

void
Set_bq25895_FORCE_ICO (uint8_t value)
{
  uint8_t wd, Reg_addr = BQ25895_REG09;

  // getting current status
   wd = Get_bq25895_FORCE_ICO ();

  //clearing the bits
  wd &= ~(0x80);

  //Updating
  wd = (value << 7) & 0x80;

  writeByte (Reg_addr, wd);
}

uint8_t
Get_bq25895_TMR2X_EN ()
{
  uint8_t rd, val, Reg_addr = BQ25895_REG09;

  rd = readByte (Reg_addr);

  //Clearing
  val &= ~(0x01);

  //Updating
  val |= (rd >> 6) & 0x01;
  return val;
}

void
Set_bq25895_TMR2X_EN (uint8_t value)
{
  uint8_t wd, Reg_addr = BQ25895_REG09;

  // getting current status
   wd = Get_bq25895_TMR2X_EN ();

  //clearing the bits
  wd &= ~(0x40);

  //Updating
  wd = (value << 6) & 0x40;

  writeByte (Reg_addr, wd);

}

void
Get_bq25895_BATFET_STAT (uint8_t BATFET_DLY, uint8_t BATFET_RST_EN)
{
  uint8_t rd, Reg_addr = BQ25895_REG09;

  rd = readByte (Reg_addr);

  BATFET_DLY = (rd >> 3) & 0x01;
  BATFET_RST_EN = (rd >> 2) & 0x01;
}

uint8_t
Get_bq25895_BATFET_DIS ()
{
  uint8_t rd, val, Reg_addr = BQ25895_REG09;

  rd = readByte (Reg_addr);

  //Clearing
  val &= ~(0x01);

  //Updating
  val |= (rd >> 5) & 0x01;
  return val;
}

void
Set_bq25895_BATFET_DIS (uint8_t value)
{
  uint8_t wd, Reg_addr = BQ25895_REG09;

  // getting current status
     wd = Get_bq25895_BATFET_DIS ();

  //clearing the bits
  wd &= ~(0x10);

  //Updating
  wd = (value << 5) & 0x10;

  writeByte (Reg_addr, wd);

}

void
Set_bq25895_BATFET_DLY (uint8_t value)
{
  uint8_t wd, Reg_addr = BQ25895_REG09;

  if (Get_bq25895_BATFET_DIS () == 0x01)
    {
      //clearing the bits
      wd &= ~(0x08);

      //Updating
      wd = (value << 3) & 0x08;

      writeByte (Reg_addr, wd);
    }
}

uint8_t
Get_bq25895_BATFET_RST_EN ()
{
  uint8_t rd, val, Reg_addr = BQ25895_REG09;

  rd = readByte (Reg_addr);

  //Clearing
  val &= ~(0x01);

  //Updating
  val |= (rd >> 2) & 0x01;
  return val;
}

void
Set_bq25895_BATFET_RST_EN (uint8_t value)
{
  uint8_t wd, Reg_addr = BQ25895_REG09;

  // getting current status
   wd = Get_bq25895_BATFET_RST_EN ();

  //clearing the bits
  wd &= ~(0x04);

  //Updating
  wd = (value << 2) & 0x04;

  writeByte (Reg_addr, wd);

}

void
Get_bq25895_PUMPX_STAT (uint8_t PUMPX_UP, uint8_t PUMPX_DN)
{
  uint8_t rd, Reg_addr = BQ25895_REG09;

  rd = readByte (Reg_addr);

  PUMPX_UP = (rd >> 1) & 0x01;
  PUMPX_DN = rd & 0x01;
}

void
Set_bq25895_PUMPX_UP (uint8_t value)
{
  uint8_t wd, Reg_addr = BQ25895_REG09;

  //clearing the bits
  wd &= ~(0x02);

  //Updating
  wd = (value << 1) & 0x02;

  writeByte (Reg_addr, wd);

}
void
Set_bq25895_PUMPX_DN (uint8_t value)
{
  uint8_t wd, Reg_addr = BQ25895_REG09;

  //clearing the bits
  wd &= ~(0x01);

  //Updating
  wd = value & 0x01;

  writeByte (Reg_addr, wd);

}
uint8_t
Get_bq25895_BOOSTV ()
{
  uint8_t rd, val, Reg_addr = BQ25895_REG0A;

  rd = readByte (Reg_addr);

  //Clearing
  val &= ~(0x0f);

  //Updating
  val |= (rd >> 4) & 0x0F;
  return (val * 64) + REG0A_BOOSTV_OS_mV;              // RESOLUTION:64mV
}

void
Set_bq25895_BOOSTV (BOOSTV value)
{
  uint8_t wd, Reg_addr = BQ25895_REG0A;

  // getting current status
   wd = Get_bq25895_BOOSTV ();

  //clearing the bits
  wd &= ~(0x01);

  //Updating
  wd = value;

  writeByte (Reg_addr, wd);
}

enum BQ25895_VBUS_STATUS
Get_bq25895_VBUS_STAT ()
{
  enum BQ25895_VBUS_STATUS rd;
  uint8_t val, Reg_addr = BQ25895_REG0B;

  val = readByte (Reg_addr);

  switch (val)
    {
    case NO_INPUT:
      rd = NO_INPUT;
      break;
    case USB_HOST_SDP:
      rd = USB_HOST_SDP;
      break;
    case USB_CDP:
      rd = USB_CDP;
      break;
    case USB_DCP:
      rd = USB_DCP;
      break;
    case ADJUSTABLE_HV_DCP:
      rd = ADJUSTABLE_HV_DCP;
      break;
    case UNKNOWN_ADAPTER:
      rd = UNKNOWN_ADAPTER;
      break;
    case NON_STANDARD_ADAPTER:
      rd = NON_STANDARD_ADAPTER;
      break;
    case OTG:
      rd = OTG;
      break;
    }
  return rd;
}

enum BQ25895_CHRG_STATUS
Get_bq25895_CHRG_STAT ()
{
  enum BQ25895_CHRG_STATUS rd;
  uint8_t val, Reg_addr = BQ25895_REG0B;

  val = readByte (Reg_addr);

  switch (val)
    {
    case NOT_CHARGING:
      rd = NOT_CHARGING;
      break;
    case PRE_CHARGE:
      rd = PRE_CHARGE;
      break;
    case FAST_CHARGING:
      rd = FAST_CHARGING;
      break;
    case CHARGE_TERMINATION_DONE:
      rd = CHARGE_TERMINATION_DONE;
      break;
    }
  return rd;
}

uint8_t
Get_bq25895_PG_STAT ()
{
  uint8_t rd, val, Reg_addr = BQ25895_REG0B;

  rd = readByte (Reg_addr);

  //Clearing
  val &= ~(0x01);

  //Updating
  val |= (rd >> 1) & 0x01;
  return val;
}

uint8_t
Get_bq25895_SDP_STAT ()
{
  uint8_t rd, val, Reg_addr = BQ25895_REG0B;

  rd = readByte (Reg_addr);

  //Clearing
  val &= ~(0x01);

  //Updating
  val |= (rd >> 1) & 0x01;
  return val;
}

uint8_t
Get_bq25895_VSYS_STAT ()
{
  uint8_t rd, val, Reg_addr = BQ25895_REG0B;

  rd = readByte (Reg_addr);

  //Clearing
  val &= ~(0x01);

  //Updating
  val |= rd & 0x01;
  return val;
}

uint8_t
Get_bq25895_WATCHDOG_FAULT ()
{
  uint8_t rd, val, Reg_addr = BQ25895_REG0C;

  rd = readByte (Reg_addr);

  //Clearing
  val &= ~(0x01);

  //Updating
  val |= (rd >> 7) & 0x01;
  return val;

}

uint8_t
Get_bq25895_BOOST_FAULT ()
{
  uint8_t rd, val, Reg_addr = BQ25895_REG0C;

  rd = readByte (Reg_addr);

  //Clearing
  val &= ~(0x01);

  //Updating
  val |= (rd >> 6) & 0x01;
  return val;
}

CHRG_FAULT
Get_bq25895_CHRG_FAULT ()
{
  CHRG_FAULT rd;
  uint8_t val, Reg_addr = BQ25895_REG0C;

  val = readByte (Reg_addr);

  switch (val)
    {
    case NORMAL:
      rd = NORMAL;
      break;
    case INPUT_FAULT:
      rd = INPUT_FAULT;
      break;
    case THERMAL_SHUTDOWN:
      rd = THERMAL_SHUTDOWN;
      break;
    case CHARGE_SAFETY_TIMER_EXPIRATION:
      rd = CHARGE_SAFETY_TIMER_EXPIRATION;
      break;
    }
  return rd;
}

uint8_t
Get_bq25895_BAT_FAULT ()
{
  uint8_t rd, val, Reg_addr = BQ25895_REG0C;

  rd = readByte (Reg_addr);

  //Clearing
  val &= ~(0x01);

  //Updating
  val |= (rd >> 3) & 0x01;
  return val;
}

NTC_FAULT
Get_bq25895_NTC_FAULT ()
{
  NTC_FAULT rd;
  uint8_t val, Reg_addr = BQ25895_REG0C;

  val = readByte (Reg_addr);

  switch (val)
    {
    case NORMALBB:
      rd = NORMALBB;
      break;
    case Buck_TS_COLD:
      rd = Buck_TS_COLD;
      break;
    case Buck_TS_HOT:
      rd = Buck_TS_HOT;
      break;
    case Boost_TS_COLD:
      rd = Boost_TS_COLD;
      break;
    case Boost_TS_HOT:
      rd = Boost_TS_HOT;
      break;
    }

  return rd;
}

void
Set_bq25895_FORCE_VINDPM (uint8_t value)
{
  uint8_t wd, Reg_addr = BQ25895_REG0D;

  // getting current status
   wd = Get_bq25895_FORCE_VINDPM ();

  //clearing the bits
  wd &= ~(0x80);

  //Updating
  wd = (value << 7) & 0x80;

  writeByte (Reg_addr, wd);

}

uint8_t
Get_bq25895_FORCE_VINDPM ()
{
  uint8_t rd, val, Reg_addr = BQ25895_REG0D;

  rd = readByte (Reg_addr);

  //Clearing
  val &= ~(0x01);

  //Updating
  val |= (rd >> 7) & 0x01;
  return val;

}

uint8_t
Get_bq25895_VINDPM ()
{

  uint8_t rd, val, Reg_addr = BQ25895_REG0D;

  if (Get_bq25895_FORCE_VINDPM () == 0x01)
    {
      rd = readByte (Reg_addr);

      //Clearing
      val &= ~(0x07f);

      //Updating
      val |= rd & 0x7f;
    }
  return (val * 100) + REG0D_VINDPM_OS_mV;        //Resolution:100mV
}

void
Set_bq25895_VINDPM (VINDPM value)
{
  uint8_t wd, Reg_addr = BQ25895_REG0D;

  // getting current status
   wd = Get_bq25895_VINDPM ();

  //clearing the bits
  wd &= ~(0x01);

  //Updating
  wd = value;

  if (Get_bq25895_FORCE_VINDPM () == 0x01)
    {
      writeByte (Reg_addr, wd);
    }
}

uint8_t
Get_bq25895_THERM_STAT ()
{
  uint8_t rd, val, Reg_addr = BQ25895_REG0E;

  rd = readByte (Reg_addr);

  //Clearing
  val &= ~(0x01);

  //Updating
  val |= (rd >> 7) & 0x01;
  return val;
}

uint8_t
Get_bq25895_ADC_Conv_BATV ()
{
  uint8_t rd, val, Reg_addr = BQ25895_REG0E;

  rd = readByte (Reg_addr);

  //Clearing
  val &= ~(0x7f);

  //Updating
  val |= rd & 0x7f;
  return (val * 20) + REG0E_BATV_OS_mV;            // RESOLUTION:20mV
}

uint8_t
Get_bq25895_ADC_Conv_SYSV ()
{
  uint8_t rd, val, Reg_addr = BQ25895_REG0F;

  rd = readByte (Reg_addr);

  //Clearing
  val &= ~(0x7f);

  //Updating
  val |= rd & 0x7f;
  return (val * 20) + REG0F_SYSV_OS_mV;            //RESOLUTION:20mV
}

uint8_t
Get_bq25895_ADC_Conv_TSV ()
{
  uint8_t rd, val, Reg_addr = BQ25895_REG10;

  rd = readByte (Reg_addr);

  //Clearing
  val &= ~(0x7f);

  //Updating
  val |= rd & 0x7f;
  return (val / 0.465) + REG10_TSPCT_OS_Percent;    //RESOLUTION:0.465%
}

uint8_t
Get_bq25895_ADC_Conv_VBUSV ()
{
  uint8_t rd, val, Reg_addr = BQ25895_REG11;

  rd = readByte (Reg_addr);

  //Clearing
  val &= ~(0x7f);

  //Updating
  val |= rd & 0x7f;
  return (val * 100) + REG11_VBUSV_OS_mV;                //RESOLUTION:100mV
}

uint8_t
Get_bq25895_ADC_Conv_ICHGR ()
{
  uint8_t rd, val, Reg_addr = BQ25895_REG12;

  rd = readByte (Reg_addr);

  //Clearing
  val &= ~(0x7f);

  //Updating
  val |= rd & 0x7f;
  return val * 50;                        // RESOLUTION:50mV
}

void
Get_bq25895_IN_REG_STAT (uint8_t VDPM_STAT, uint8_t IDPM_STAT)
{
  uint8_t rd, Reg_addr = BQ25895_REG13;

  rd = readByte (Reg_addr);

  VDPM_STAT = (rd >> 7) & 0x01;
  IDPM_STAT = (rd >> 6) & 0x01;
}

uint8_t
Get_bq25895_IDPM_LIM ()
{
  uint8_t rd, val, Reg_addr = BQ25895_REG13;

  rd = readByte (Reg_addr);

  //Clearing
  val &= ~(0x3f);

  //Updating
  val |= rd & 0x3f;
  return (val * 50) + REG13_IDPM_LIM_OS_mV;             // RESOLUTION:50mV
}

uint8_t
Get_bq25895_REG_RST ()
{
  uint8_t rd, val, Reg_addr = BQ25895_REG14;

  rd = readByte (Reg_addr);

  //Clearing
  val &= ~(0x01);

  //Updating
  val |= (rd >> 7) & 0x01;
  return val;
}

void
Set_bq25895_REG_RST (uint8_t value)
{
  uint8_t wd, Reg_addr = BQ25895_REG14;

  // getting current status
   wd = Get_bq25895_REG_RST ();

  //clearing the bits
  wd &= ~(0x80);

  //Updating
  wd = (value << 7) & 0x80;

  writeByte (Reg_addr, wd);

}

void
Get_bq25895_DEVICE_INFO (uint8_t PN, uint8_t DEV_REV)
{
  uint8_t rd, Reg_addr = BQ25895_REG14;

  rd = readByte (Reg_addr);

  PN = (rd >> 3) & 0x07;
  DEV_REV = rd & 0x03;
}

uint8_t
Get_bq25895_ICO_OPTIMIZED ()
{
  uint8_t rd, val, Reg_addr = BQ25895_REG14;

  rd = readByte (Reg_addr);

  //Clearing
  val &= ~(0x01);

  //Updating
  val |= (rd >> 6) & 0x01;
  return val;
}

void
Set_bq25895_ICO_OPTIMIZED (uint8_t value)
{
  uint8_t wd, Reg_addr = BQ25895_REG14;

  // getting current status
  wd = Get_bq25895_ICO_OPTIMIZED ();

  //clearing the bits
  wd &= ~(0x40);

  //Updating
  wd = (value << 6) & 0x40;

  writeByte (Reg_addr, wd);

}

uint8_t
Get_bq25895_TS_PROFILE ()
{
  uint8_t rd, val, Reg_addr = BQ25895_REG14;

  rd = readByte (Reg_addr);

  //Clearing
  val &= ~(0x01);

  //Updating
  val |= (rd >> 2) & 0x01;
  return val;
}


