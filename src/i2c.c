/*******************************************************************************
  I2C/SMB Driver:  Implementation

  File Name:
    i2c.c

  Summary:
    Implementation of MCU specific I2C functions.

  Description:
    None
 *******************************************************************************/

// Included Files

#include "smb_0.h"
#include "i2c.h"

// *****************************************************************************
// Global holder for SMBus data
// All receive data is written here
SI_SEGMENT_VARIABLE(SMB_DATA_IN[NUM_BYTES_RD],
                    uint8_t,
                    EFM8PDL_SMB0_RX_BUFTYPE);

// Global holder for SMBus data.
// All transmit data is read from here
SI_SEGMENT_VARIABLE(SMB_DATA_OUT[NUM_BYTES_WR],
                    uint8_t,
                    EFM8PDL_SMB0_TX_BUFTYPE);
// *****************************************************************************

// I2C Functions

uint8_t readByte (uint8_t Reg_addr)
{
  SMB_DATA_OUT[0] = Reg_addr;
  SMB0_transfer (TARGET_ADDRESS, SMB_DATA_OUT, SMB_DATA_IN, 0, 1);
  return SMB_DATA_OUT[0];
}


void ReadArray (uint8_t Reg_addr, uint8_t Byte_RD, uint8_t *RxData)
{
  uint8_t i;
  SMB_DATA_OUT[0] = Reg_addr;
  SMB0_transfer (TARGET_ADDRESS, SMB_DATA_OUT, SMB_DATA_IN, Byte_RD, 1);
  for (i = 0; i < Byte_RD; i++)
    {
      //Updating Data
      RxData[i] = SMB_DATA_IN[i];
    }
}

void writeByte (uint8_t Reg_addr, uint8_t Txdata)
{
  SMB_DATA_OUT[0] = Reg_addr;
  SMB_DATA_OUT[1] = Txdata;
  SMB0_transfer (TARGET_ADDRESS, SMB_DATA_OUT, SMB_DATA_IN, 0, 2);
}

void WriteArray (uint8_t Reg_addr, uint8_t Byte_WR , uint8_t *Txdata)
{
  uint8_t i;
  SMB_DATA_OUT[0] = Reg_addr;
  for (i = 1; i < Byte_WR+1; i++)
    {
      // Updating I2C bus
      SMB_DATA_OUT[i] = Txdata[i];
    }
  SMB0_transfer (TARGET_ADDRESS, SMB_DATA_OUT, SMB_DATA_IN, 0, Byte_WR+1);
}
