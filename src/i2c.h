/*******************************************************************************
  I2C/SMB Driver:  Header File

  File Name:
    i2c.h

  Summary:
    This header file contains the MCU specific I2C definitions and declarations.

  Description:
    .
 *******************************************************************************/

#ifndef SRC_I2C_H_
#define SRC_I2C_H_


#define TARGET_ADDRESS 0x6A         //device address


#define  NUM_BYTES_WR   3           // Number of bytes to write
#define  NUM_BYTES_RD   3           // Number of bytes to read

// I2C Functions

// *****************************************************************************

// I2C Byte Read

uint8_t
readByte (uint8_t Reg_addr);

// I2C Read Array

void
ReadArray (uint8_t Reg_addr, uint8_t Byte_RD, uint8_t *RxData);

// I2C Byte Write

void
writeByte (uint8_t Reg_addr, uint8_t Txdata);

// I2C Write Array

void
WriteArray (uint8_t Reg_addr, uint8_t Byte_WR, uint8_t *Txdata);

#endif /* SRC_I2C_H_ */
