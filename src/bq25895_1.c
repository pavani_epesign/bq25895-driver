/*******************************************************************************
 BQ25895 I2C Driver: Implementation

 File Name:
 bq25895_1.c

 Summary:

 This Source file provides Implementation of the Initializations and Operations
 of the BQ25895 Battery Charger.

 Description:
 None.
 *******************************************************************************/
// *****************************************************************************

//Included Files

#include "bq25895.h"
#include "bq25895_1.h"

// *****************************************************************************

void
bq25895_CHRG_Enable ()
{
  Set_bq25895_CHG_CONFIG (0x01);
  Set_bq25895_VREG (VREG_512mV);
  Set_bq25895_ICHG (ICHG_2048mA);
  Set_bq25895_VREG (VREG_256mV | VREG_64mV | VREG_32mV | VREG_16mV);
  Set_bq25895_ITERM (ITERM_256mA);
  Set_bq25895_FORCE_ICO (0x01);
}

void
bq25895_CHRG_disable ()
{

  Set_bq25895_CHG_CONFIG (0x00);
  Set_bq25895_ICHG (ICHG_0mA);
  Set_bq25895_FORCE_ICO (0x00);
}
void
bq25895_BAT_CHRG_CYCLE_RST ()
{

  Set_bq25895_VREG (VREG_256mV | VREG_64mV | VREG_32mV | VREG_16mV);
  Set_bq25895_ICHG (ICHG_2048mA);
  Set_bq25895_ITERM (ITERM_128mA);
  Set_bq25895_IPRECHG (IPRECHG_256mA);
  Set_bq25895_TIMER_STAT (0x01, 0x02);
}

void
Set_bq25895_Default_mode ()
{
  Set_bq25895_WD_RST (0x01);
  Set_bq25895_REG_RST (0x01);
}

void
Set_bq25895_Host_mode ()
{
  uint8_t val=0x00;
  val = Get_bq25895_WATCHDOG_FAULT ();

  // Checking watchdog fault status
  if (val == 0x01)
    {
      Set_bq25895_WD_RST (0x01);
    }

  //disabling watchdog timer
  Set_bq25895_WATCHDOG (0x00);
}

uint8_t
Get_bq25895_DPM_Mode ()
{
  uint8_t val=0x00;
  uint8_t VDPM_STAT, IDPM_STAT;

  // Getting VINDPM Status and IINDPM Status
  Get_bq25895_IN_REG_STAT (VDPM_STAT, IDPM_STAT);
  val = VDPM_STAT | IDPM_STAT;
  return val;
}

void
Get_bq25895_input_status (enum BQ25895_VBUS_STATUS vbus_stat, uint8_t curr)
{
  vbus_stat = Get_bq25895_VBUS_STAT ();

  // Reading input current limit values from applied input source.

  if (Get_bq25895_ICO_EN () == 0x01)
    {
      curr = Get_bq25895_IDPM_LIM ();
    }
  else
    {

      curr = Get_bq25895_IN_ILIM (1);

    }

}

void
EN_bq25895_MXCHRGhandshake ()
{

  Set_bq25895_InSource_HDCP (1);
}

void
DIS_bq25895_MXCHRGhandshake ()
{

  Set_bq25895_InSource_HDCP (0);
}

