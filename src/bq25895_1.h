/*******************************************************************************
 BQ25895 I2C Driver: Defines Header File

 File Name:
 bq25895_1.h

 Summary:
 This header file contains object declarations used in the API.
 This also contains device specific defines.
 This header file provides the Function Prototypes for the BQ25895 Battery
 Charger.

 Description:
 None.
 *******************************************************************************/

#ifndef SRC_BQ25895_1_H_
#define SRC_BQ25895_1_H_

// *****************************************************************************

// Included Files

#include <stdint.h>

// *****************************************************************************

// Function Prototypes

/*  This function provides the input status of BQ25895 Charger.It gives Input
 * current limit values (IDPM_LIM or IN_ILIM).And also It gives the type of
 * input source. */
void
Get_bq25895_input_status (enum BQ25895_VBUS_STATUS vbus_stat, uint8_t curr);

/*  This function enables BQ25895 Charger. */
void
bq25895_CHRG_Enable ();

/*  This function disables BQ25895 Charger. */
void
bq25895_CHRG_disable ();

/*  This function is for resetting automatic charging cycle of BQ25895 Charger
 * by setting default parameters.*/
void
bq25895_BAT_CHRG_CYCLE_RST ();
void

/*  This function is used to make the charger works in Default mode . */
Set_bq25895_Default_mode ();

/* This function is used to make the charger works in Host mode. */
void
Set_bq25895_Host_mode ();

/* This function provides the DPM status of BQ25895 Charger.It gives input
 * voltage VINDPM Status and Input current IINDPM Status.*/
uint8_t
Get_bq25895_DPM_Mode ();


/*  This function enables BQ25895 Charger Maximum Charge or High Voltage
 * adapter Handshaking . */
void
EN_bq25895_MXCHRGhandshake ();

/*  This function disables BQ25895 Charger Maximum Charge or High Voltage
 * adapter Handshaking . */
void
DIS_bq25895_MXCHRGhandshake ();

#endif /* SRC_BQ25895_1_H_ */
